export class Comment {
  id: number;
  owner: string;
  time: string;
  imageUrl: string;
  text: string;
  quantityLiked: number;
  quantityLikedActive: string;
  /* Comment with replies */
  repliedComment: number;  
}