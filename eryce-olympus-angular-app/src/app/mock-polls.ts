import { Poll } from './poll';

export const POLLS: Poll[] = [
  { 
    id: 1,
    choice: 'Thomas Bale',
    votedPercent: 62, 
    votedPeople: 12
  },
  { 
    id: 2,
    choice: 'Ben Robertson',
    votedPercent: 27, 
    votedPeople: 7
  },
  { 
    id: 3,
    choice: 'Michael Streiton',
    votedPercent: 11, 
    votedPeople: 2
  }    
];  
