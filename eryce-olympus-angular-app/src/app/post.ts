export class Post {
  id: number;
  owner: string;
  time: string;
  /* Control buttons */
  controlFeatured: number;
  controlFeaturedActive: string;
  controlLikedActive: string;
  controlCommentedActive: string;
  controlSharedActive: string;
  /* Interaction */
  quantityLikedActive: string;
  quantityLikedBruto: number;
  quantityLikedNeto: number;
  whoLikedPerson1: string;
  whoLikedPerson2: string;
  quantityCommented: number;
  quantityShared: number;
  /* Post text */
  text: string;
  /* Post shared a link */
  sharedALink: number;
  linkUrl: string;
  /* Post shared a video */
  sharedAVideo: number;
  videoTitle: string;
  videoImgUrl: string;
  videoText: string;
  videoLinkText: string;
  videoLink: string;
  /* Post shared a photo */
  sharedAPhoto: number;
  sharedPhotoOwner: string;
  sharedPhotoOwnerLink: string;
  sharedPhotoOwnerImgUrl: string;
  sharedPhotoLink: string;
  sharedPhotoUrl: string;
  sharedPhotoPostTime: string;
  sharedPhotoPostText: string;
  /* Post with Comments */
  commentedPost: number;
}