export class Reply {
  id: number;
  owner: string;
  time: string;
  imageUrl: string;
  text: string;
  quantityLiked: number;
  quantityLikedActive: string;
}