import { Page } from './page';

export const PAGES: Page[] = [
  { 
    id: 1,
    pageTitle: 'The Marina Bar',
    pageType: 'Restaurant / Bar',
    pageImgUrl: 'assets/img/pages/page1.jpg'
  },
  { 
    id: 2,
    pageTitle: 'Tapronus Rock',
    pageType: 'Rock Band',
    pageImgUrl: 'assets/img/pages/page2.jpg'
  },
  { 
    id: 3,
    pageTitle: 'Pixel Digital Design',
    pageType: 'Company',
    pageImgUrl: 'assets/img/pages/page3.jpg'
  },
  { 
    id: 4,
    pageTitle: 'Ray Bar & Grill',
    pageType: 'Restaurant / Bar',
    pageImgUrl: 'assets/img/pages/page4.jpg'
  },
  { 
    id: 5,
    pageTitle: 'Crimson Agency',
    pageType: 'Company',
    pageImgUrl: 'assets/img/pages/page5.jpg'
  }        
];  