export class Playlist {
  id: number;
  title: string;
  artist: string;
  duration: string;
  imageUrl: string;
}