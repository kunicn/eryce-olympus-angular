import { Component, OnInit } from '@angular/core';
import { Badge } from '../badge';
import { BADGES } from '../mock-badges';
import { Playlist } from '../playlist';
import { PLAYLISTS } from '../mock-playlists';
import { Tweet } from '../tweet';
import { TWEETS } from '../mock-tweets';
import { Video } from '../video';
import { VIDEOS } from '../mock-videos';

@Component({
  selector: 'app-profile-sidebar-left',
  templateUrl: './profile-sidebar-left.component.html',
  styleUrls: ['./profile-sidebar-left.component.css']
})

export class ProfileSidebarLeftComponent implements OnInit {

  badges = BADGES;

  playlists = PLAYLISTS;

  tweets = TWEETS;

  videos = VIDEOS;

  constructor() { }

  ngOnInit() {
  }

}
