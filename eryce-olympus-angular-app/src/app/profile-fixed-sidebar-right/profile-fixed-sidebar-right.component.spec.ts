import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileFixedSidebarRightComponent } from './profile-fixed-sidebar-right.component';

describe('ProfileFixedSidebarRightComponent', () => {
  let component: ProfileFixedSidebarRightComponent;
  let fixture: ComponentFixture<ProfileFixedSidebarRightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileFixedSidebarRightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFixedSidebarRightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
