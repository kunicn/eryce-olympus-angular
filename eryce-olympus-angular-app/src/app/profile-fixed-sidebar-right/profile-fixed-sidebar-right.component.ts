import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { USERS } from '../mock-users';

@Component({
  selector: 'app-profile-fixed-sidebar-right',
  templateUrl: './profile-fixed-sidebar-right.component.html',
  styleUrls: ['./profile-fixed-sidebar-right.component.css']
})

export class ProfileFixedSidebarRightComponent implements OnInit {

  users = USERS;

  constructor() { }

  ngOnInit() {
  }

}