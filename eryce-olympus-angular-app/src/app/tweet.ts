export class Tweet {
  id: number;
  twitterName: string;
  twitterNick: string;
  tweet: string;
  tweetLink: string;
  tweetTime: string;
  twitterThumbUrl: string;
}