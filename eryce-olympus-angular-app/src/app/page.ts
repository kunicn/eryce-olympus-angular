export class Page {
  id: number;
  pageTitle: string;
  pageType:string;
  pageImgUrl: string;
}