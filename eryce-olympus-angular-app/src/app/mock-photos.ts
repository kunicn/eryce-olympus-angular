import { Photo } from './photo';

export const PHOTOS: Photo[] = [
  { 
    id: 1,
    photoThumbUrl: 'assets/img/photos/photo1.jpg'
  },
  { 
    id: 2,
    photoThumbUrl: 'assets/img/photos/photo2.jpg'
  },
  { 
    id: 3,
    photoThumbUrl: 'assets/img/photos/photo3.jpg'
  },
  { 
    id: 4,
    photoThumbUrl: 'assets/img/photos/photo4.jpg'
  },
  { 
    id: 5,
    photoThumbUrl: 'assets/img/photos/photo5.jpg'
  },
  { 
    id: 6,
    photoThumbUrl: 'assets/img/photos/photo6.jpg'
  },
  { 
    id: 7,
    photoThumbUrl: 'assets/img/photos/photo7.jpg'
  },
  { 
    id: 8,
    photoThumbUrl: 'assets/img/photos/photo8.jpg'
  },
  { 
    id: 9,
    photoThumbUrl: 'assets/img/photos/photo9.jpg'
  }
];  