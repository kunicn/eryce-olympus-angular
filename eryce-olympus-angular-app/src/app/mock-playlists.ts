import { Playlist } from './playlist';

export const PLAYLISTS: Playlist[] = [
  { 
    id: 1,
    title: 'The Past Starts Slow...',
    artist: 'System of a Revenge',
    duration: '3:22',
    imageUrl: 'assets/img/playlist/playlist1.jpg'
  },
  { 
    id: 2,
    title: 'The Pretender',
    artist: 'Kung Fighters',
    duration: '5:48',
    imageUrl: 'assets/img/playlist/playlist2.jpg'
  },
  { 
    id: 3,
    title: 'Blood Brothers',
    artist: 'Iron Maid',
    duration: '3:06',
    imageUrl: 'assets/img/playlist/playlist3.jpg'
  },
  { 
    id: 4,
    title: 'Seven Nation Army',
    artist: 'The Black Stripes',
    duration: '6:17',
    imageUrl: 'assets/img/playlist/playlist4.jpg'
  },
  { 
    id: 5,
    title: 'Killer Queen',
    artist: 'Archiduke',
    duration: '5:40',
    imageUrl: 'assets/img/playlist/playlist5.jpg'
  }        
];    