import { Badge } from './badge';

export const BADGES: Badge[] = [
  { 
    id: 1,
    badgeThumbUrl: 'assets/img/badges/badge1.png',
    badgeQuantity: 2
  },
  { 
    id: 2,
    badgeThumbUrl: 'assets/img/badges/badge2.png',
    badgeQuantity: 0
  },
  { 
    id: 3,
    badgeThumbUrl: 'assets/img/badges/badge3.png',
    badgeQuantity: 4
  },
  { 
    id: 4,
    badgeThumbUrl: 'assets/img/badges/badge4.png',
    badgeQuantity: 0
  },
  { 
    id: 5,
    badgeThumbUrl: 'assets/img/badges/badge5.png',
    badgeQuantity: 0
  },
  { 
    id: 6,
    badgeThumbUrl: 'assets/img/badges/badge6.png',
    badgeQuantity: 0
  },
  { 
    id: 7,
    badgeThumbUrl: 'assets/img/badges/badge7.png',
    badgeQuantity: 0
  },
  { 
    id: 8,
    badgeThumbUrl: 'assets/img/badges/badge8.png',
    badgeQuantity: 4
  },
  { 
    id: 9,
    badgeThumbUrl: 'assets/img/badges/badge9.png',
    badgeQuantity: 0
  },
  { 
    id: 10,
    badgeThumbUrl: 'assets/img/badges/badge10.png',
    badgeQuantity: 0
  }
];  