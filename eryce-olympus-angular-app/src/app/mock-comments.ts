import { Comment } from './comment';

export const COMMENTS: Comment[] = [
  { 
    id: 1,
    owner: 'Elaine Dreyfuss',
    time: '5 mins ago',
    imageUrl: 'assets/img/users/elaine-dreyfuss-thumb.jpg',
    text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.',
    quantityLiked: 8,
    quantityLikedActive: '',
    repliedComment: 0     
  },
  { 
    id: 2,
    owner: 'Green Goo Rock',
    time: '1 hour ago',
    imageUrl: 'assets/img/users/green-goo-rock-thumb.jpg',
    text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.',
    quantityLiked: 5,
    quantityLikedActive: 'eo-active',
    repliedComment: 1    
  },
  { 
    id: 3,
    owner: 'Chris Greyson',
    time: '1 hour ago',
    imageUrl: 'assets/img/users/chris-greyson-thumb.jpg',
    text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.',
    quantityLiked: 7,
    quantityLikedActive: '',
    repliedComment: 0    
  }    
];
