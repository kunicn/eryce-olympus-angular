import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileFixedSidebarLeftComponent } from './profile-fixed-sidebar-left.component';

describe('ProfileFixedSidebarLeftComponent', () => {
  let component: ProfileFixedSidebarLeftComponent;
  let fixture: ComponentFixture<ProfileFixedSidebarLeftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileFixedSidebarLeftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFixedSidebarLeftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
