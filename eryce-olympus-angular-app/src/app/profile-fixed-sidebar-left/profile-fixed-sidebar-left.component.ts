import { Component, OnInit } from '@angular/core';
import { Icon } from '../icon';
import { ICONS } from '../mock-icons';

@Component({
  selector: 'app-profile-fixed-sidebar-left',
  templateUrl: './profile-fixed-sidebar-left.component.html',
  styleUrls: ['./profile-fixed-sidebar-left.component.css']
})

export class ProfileFixedSidebarLeftComponent implements OnInit {

  icons = ICONS;

  constructor() { }

  ngOnInit() {
  }

}
