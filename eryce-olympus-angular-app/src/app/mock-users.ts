import { User } from './user';

export const USERS: User[] = [
  { 
    id: 1,
    name: 'James Spiegel',
    firstName: 'James',
    nickname: 'Space Cowboy', 
    location: 'San Francisco, CA',
    status: 'eo-badge-online',
    imageUrl: 'assets/img/users/james-spiegel.jpg',
    imageThumbUrl: 'assets/img/users/james-spiegel-thumb.jpg',
  },
  { 
    id: 2,
    name: 'Elaine Dreyfuss',
    firstName: 'Elaine',
    nickname: 'Cheery Pops', 
    location: 'New York City, NY',
    status: 'eo-badge-online',
    imageUrl: 'assets/img/users/elaine-dreyfuss.jpeg',
    imageThumbUrl: 'assets/img/users/elaine-dreyfuss-thumb.jpg'
  },
  { 
    id: 3,
    name: 'Green Goo Rock',
    firstName: 'Green',
    nickname: 'CrescentPrime', 
    location: 'San Francisco, CA',
    status: 'eo-badge-away',
    imageUrl: 'assets/img/users/green-goo-rock.jpg',
    imageThumbUrl: 'assets/img/users/green-goo-rock-thumb.jpg'
  },
  { 
    id: 4,
    name: 'Diana Jameson',
    firstName: 'Diana',
    nickname: 'Silly Skipper', 
    location: 'Los Angeles, CA',
    status: 'eo-badge-offline',
    imageUrl: 'assets/img/users/diana-jameson.jpeg',
    imageThumbUrl: 'assets/img/users/diana-jameson-thumb.jpg'
  },  
  { 
    id: 5,
    name: 'Nicholas Grisom',
    firstName: 'Nicholas',
    nickname: 'Shark Prince', 
    location: 'New York City, NY',
    status: 'eo-badge-online',
    imageUrl: 'assets/img/users/nicholas-grisom.png',
    imageThumbUrl: 'assets/img/users/nicholas-grisom-thumb.jpg'
  },
  { 
    id: 6,
    name: 'Chris Greyson',
    firstName: 'Chris',
    nickname: 'Wonderman', 
    location: 'New York City, NY',
    status: 'eo-badge-away',
    imageUrl: 'assets/img/users/chris-greyson.jpg',
    imageThumbUrl: 'assets/img/users/chris-greyson-thumb.jpg'
  },
  { 
    id: 7,
    name: 'Jenny Whittington',
    firstName: 'Jenny',
    nickname: 'Coocoo Beauty', 
    location: 'San Francisco, CA',
    status: 'eo-badge-online',
    imageUrl: 'assets/img/users/jenny-whittington.jpg',
    imageThumbUrl: 'assets/img/users/jenny-whittington-thumb.jpg'
  },
  { 
    id: 8,
    name: 'Robert McCarthy',
    firstName: 'Robert',
    nickname: 'Boogie Prince', 
    location: 'Los Angeles, CA',
    status: 'eo-badge-online',
    imageUrl: 'assets/img/users/robert-mccarthy.jpg',
    imageThumbUrl: 'assets/img/users/robert-mccarthy-thumb.jpg'
  },
  { 
    id: 9,
    name: 'Olivia Palermo',
    firstName: 'Olivia',
    nickname: 'Fancy Vanilla', 
    location: 'Los Angeles, CA',
    status: 'eo-badge-online',
    imageUrl: 'assets/img/users/olivia-palermo.jpg',
    imageThumbUrl: 'assets/img/users/olivia-palermo-thumb.jpg'
  },
  { 
    id: 10,
    name: 'Stefano Agostinelli',
    firstName: 'Stefano',
    nickname: 'Crystal Bear', 
    location: 'San Francisco, CA',
    status: 'eo-badge-online',
    imageUrl: 'assets/img/users/stefano-agostinelli.jpeg',
    imageThumbUrl: 'assets/img/users/stefano-agostinelli-thumb.jpg'
  }     

];