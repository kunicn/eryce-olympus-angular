import { Reply } from './reply';

export const REPLIES: Reply[] = [
  { 
    id: 1,
    owner: 'Diana Jameson',
    time: '39 mins ago',
    imageUrl: 'assets/img/users/diana-jameson-thumb.jpg',
    text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.',
    quantityLiked: 2,
    quantityLikedActive: ''     
  },
  { 
    id: 2,
    owner: 'Nicholas Grisom',
    time: '24 mins ago',
    imageUrl: 'assets/img/users/nicholas-grisom-thumb.jpg',
    text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.',
    quantityLiked: 0,
    quantityLikedActive: ''    
  } 
];