import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from './../environments/environment';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './/app-routing.module';
import { RouterModule } from '@angular/router';

import { LoginPageComponent } from './login-page/login-page.component';
import { LoginNavbarComponent } from './login-navbar/login-navbar.component';
import { LoginMainComponent } from './login-main/login-main.component';

import { ProfilePageComponent } from './profile-page/profile-page.component';
import { NgbdTabsetOrientationComponent } from './ngbd-tabset-orientation/ngbd-tabset-orientation.component';
import { ProfileNavbarComponent } from './profile-navbar/profile-navbar.component';
import { ProfileFixedSidebarLeftComponent } from './profile-fixed-sidebar-left/profile-fixed-sidebar-left.component';
import { ProfileFixedSidebarRightComponent } from './profile-fixed-sidebar-right/profile-fixed-sidebar-right.component';
import { ProfileHeaderComponent } from './profile-header/profile-header.component';
import { ProfileSidebarLeftComponent } from './profile-sidebar-left/profile-sidebar-left.component';
import { ProfileSidebarRightComponent } from './profile-sidebar-right/profile-sidebar-right.component';
import { ProfileMainCenterComponent } from './profile-main-center/profile-main-center.component';
import { PostComponent } from './post/post.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    LoginNavbarComponent,
    LoginMainComponent,
    ProfilePageComponent,
    NgbdTabsetOrientationComponent,
    ProfileNavbarComponent,
    ProfileFixedSidebarLeftComponent,
    ProfileFixedSidebarRightComponent,
    ProfileHeaderComponent,
    ProfileSidebarLeftComponent,
    ProfileSidebarRightComponent,
    ProfileMainCenterComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    RouterModule.forRoot([
      {
        path: 'profile',
        component: ProfilePageComponent
      },        
      {
        path: '',
        component: LoginPageComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
