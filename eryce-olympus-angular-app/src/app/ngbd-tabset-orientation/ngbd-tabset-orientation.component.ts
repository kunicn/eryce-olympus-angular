import { Component, OnInit } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ngbd-tabset-orientation',
  templateUrl: './ngbd-tabset-orientation.component.html',
  styleUrls: ['./ngbd-tabset-orientation.component.css']
})
export class NgbdTabsetOrientationComponent implements OnInit {

  currentOrientation = 'horizontal';
  constructor() { }

  ngOnInit() {
  }

}
