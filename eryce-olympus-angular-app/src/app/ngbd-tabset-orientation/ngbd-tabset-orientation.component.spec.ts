import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgbdTabsetOrientationComponent } from './ngbd-tabset-orientation.component';

describe('NgbdTabsetOrientationComponent', () => {
  let component: NgbdTabsetOrientationComponent;
  let fixture: ComponentFixture<NgbdTabsetOrientationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgbdTabsetOrientationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgbdTabsetOrientationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
