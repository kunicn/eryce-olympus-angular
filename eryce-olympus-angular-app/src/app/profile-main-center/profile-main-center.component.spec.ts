import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileMainCenterComponent } from './profile-main-center.component';

describe('ProfileMainCenterComponent', () => {
  let component: ProfileMainCenterComponent;
  let fixture: ComponentFixture<ProfileMainCenterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileMainCenterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileMainCenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
