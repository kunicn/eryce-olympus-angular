export class Video {
  id: number;
  videoTitleAndArtist: string;
  videoDuration: string;
  videoImageUrl: string;
  videoUrl: string;
}