import { Video } from './video';

export const VIDEOS: Video[] = [
  {
    id: 1,
    videoTitleAndArtist: 'System of a Revenge - Hypnotize...',
    videoDuration: '3:25',
    videoImageUrl: 'assets/img/videos/video1.jpg',
    videoUrl: 'https://vimeo.com/ondemand/viewfromabluemoon4k/147865858'
  },
  {
    id: 2,
    videoTitleAndArtist: 'Green Goo - Live at Dan’s Arena',
    videoDuration: '5:48',
    videoImageUrl: 'assets/img/videos/video2.jpg',
    videoUrl: 'https://youtube.com/watch?v=excVFQ2TWig'
  }
];