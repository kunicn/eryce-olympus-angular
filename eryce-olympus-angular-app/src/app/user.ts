export class User {
  id: number;
  name: string;
  firstName: string;
  nickname: string;
  location: string;
  status: string;
  imageUrl: string;
  imageThumbUrl: string;
}