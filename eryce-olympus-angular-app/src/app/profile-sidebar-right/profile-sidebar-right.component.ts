import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { USERS } from '../mock-users';
import { Photo } from '../photo';
import { PHOTOS } from '../mock-photos';
import { Blog } from '../blog';
import { BLOGS } from '../mock-blogs';
import { Page } from '../page';
import { PAGES } from '../mock-pages';

@Component({
  selector: 'app-profile-sidebar-right',
  templateUrl: './profile-sidebar-right.component.html',
  styleUrls: ['./profile-sidebar-right.component.css']
})
export class ProfileSidebarRightComponent implements OnInit {

  users = USERS;

  photos = PHOTOS;

  blogs = BLOGS;

  pages = PAGES;
  
  constructor() { }

  ngOnInit() {
  }

}
