export class Badge {
  id: number;
  badgeThumbUrl: string;
  badgeQuantity: number;
}