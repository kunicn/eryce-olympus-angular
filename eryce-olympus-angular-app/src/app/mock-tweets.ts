import { Tweet } from './tweet';

export const TWEETS: Tweet[] = [
  { 
    id: 1,
    twitterName: 'Space Cowboy',
    twitterNick: '@james_spiegelOK',
    tweet: 'Tomorrow with the agency we will run 5 km for charity. Come and give us your support!',
    tweetLink: '#Daydream5K',
    tweetTime: '2 hours ago',
    twitterThumbUrl: 'assets/img/twitter/james-spiegel.png',
  },
  { 
    id: 2,
    twitterName: 'Space Cowboy',
    twitterNick: '@james_spiegelOK',
    tweet: 'Check out the new website of “The Bebop Bar”!',
    tweetLink: 'bytle/thbp53f',
    tweetTime: '16 hours ago',
    twitterThumbUrl: 'assets/img/twitter/james-spiegel.png',
  },
  { 
    id: 3,
    twitterName: 'Space Cowboy',
    twitterNick: '@james_spiegelOK',
    tweet: 'The Sunday is the annual agency camping trip and I still haven’t got a tent',
    tweetLink: '#TheWild #Indoors',
    tweetTime: 'Yesterday',
    twitterThumbUrl: 'assets/img/twitter/james-spiegel.png',
  }  
];  