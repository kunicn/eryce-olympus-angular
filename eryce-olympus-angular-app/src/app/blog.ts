export class Blog {
  id: number;
  title: string;
  text: string;
  time: string;
}