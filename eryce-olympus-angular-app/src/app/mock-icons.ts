import { Icon } from './icon';

export const ICONS: Icon[] = [
  { icon_name: 'view_headline' },
  { icon_name: 'list_alt' },
  { icon_name: 'star' },
  { icon_name: 'sentiment_satisfied_alt' },
  { icon_name: 'headset' },
  { icon_name: 'wb_sunny' },
  { icon_name: 'date_range' },
  { icon_name: 'donut_large' },
  { icon_name: 'cake' },
  { icon_name: 'trending_up' },
  { icon_name: 'subtitles' }   
];