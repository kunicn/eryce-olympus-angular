import { Blog } from './blog';

export const BLOGS: Blog[] = [
  { 
    id: 1,
    title: 'My Perfect Vacations in South America and Europe',
    text: 'Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut labore et.',
    time: '7 hours ago',
  },
  { 
    id: 2,
    title: 'The Big Experience of Travelling Alone',
    text: 'Lorem ipsum dolor sit amet, consect adipisicing elit, sed do eiusmod por incidid ut labore et.',
    time: 'March 18th, at 6:52pm',
  }  
];  