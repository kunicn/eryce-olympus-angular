export class Poll {
  id: number;
  choice: string;
  votedPercent: number;
  votedPeople: number;
}
