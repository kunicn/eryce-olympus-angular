import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { USERS } from '../mock-users';
import { Post } from '../post';
import { POSTS } from '../mock-posts';
import { Comment } from '../comment';
import { COMMENTS } from '../mock-comments';
import { Reply } from '../reply';
import { REPLIES } from '../mock-replies';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  users = USERS;
  posts = POSTS;
  comments = COMMENTS;
  replies = REPLIES;

  constructor() { }

  ngOnInit() {
  }

}
